/*
 * scheduler.h
 *
 *  Created on: Dec 14, 2018
 *      Author: j9497
 */

#ifndef SRC_SCHEDULER_SCHEDULER_H_
#define SRC_SCHEDULER_SCHEDULER_H_

#include <stddef.h>
#include <stdint.h>
#include <xil_printf.h>
#include <limits.h>
#include <stdlib.h>

#include "../task/task.h"

typedef struct scheduler {
	Task *tasks;
	Task *current;

	uint8_t max_priority;
	uint8_t curr_priority;

	uint32_t ticks;
} Scheduler;

void sch_reset(Scheduler *scheduler);
Task *sch_tasks();

Task *sch_get_task(uint32_t pid);
void sch_add_task(Task *task);
void sch_remove_task(uint32_t pid);
void sch_disable_task();
void sch_enable_task();


Task *sch_next();
uint32_t sch_ticks();
void sch_tick();
void sch_init();



#endif /* SRC_SCHEDULER_SCHEDULER_H_ */
