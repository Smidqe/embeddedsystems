/*
 * scheduler.c
 *
 *  Created on: Dec 7, 2018
 *      Author: j9497
 */

#include "scheduler.h"
#include <stdio.h>
#include "stdlib.h"
#include "../interrupts/interrupts.h"
#include "../uart/uart.h"
#include "../semaphore/semaphore.h"

Scheduler *scheduler;

//select the next runnable task
Task *sch_next() {
	Task *task = NULL;
	Task *current = scheduler->tasks;
	uint32_t max_priority = 0;

	while (current) {

		//something happened, is for debugging
		if (!current)
			return NULL;

		//check if the task is disabled
		if (task_get_flag(current, TASK_FLAG_DISABLED)) {
			current = current->next;
			continue;
		}

		//check current task has higher priority than the previously selected
		if (current->priority > max_priority && task_get_flag(current, TASK_FLAG_RUN_NEXT)) {
			task = current;
			max_priority = current->priority;
		}

		if (task)
			break;

		current = current->next;
	}

	return task;
}


void sch_add_task(Task *task) {
	Task *current = scheduler->tasks;

	//check if the scheduler has no tasks
	if (!scheduler->tasks) {
		scheduler->tasks = task;
		scheduler->current = task;
		return;
	}

	//get the last task in the linked list
	while (current->next)
		current = current->next;

	//add task as next
	current->next = task;
}

void sch_remove_task(uint32_t pid) {
	Task *current = scheduler->tasks;
	Task *prev = NULL;

	//loop over the tasks
	while (current) {

		//check if the pid matches
		if (current->pid == pid) {
			if (prev == NULL)
				scheduler->tasks = current->next;
			else
				prev->next = current->next;

			break;
		}

		current = current->next;
	}
}


//check if task is running
int sch_task_running(Scheduler *scheduler) {
	return scheduler->current != NULL;
}

//get current running task
Task *sch_current(Scheduler *scheduler) {
	return scheduler->current;
}

//get scheduler ticks
uint32_t sch_ticks() {
	return scheduler->ticks;
}

//tick function
void sch_tick() {
	Task *current = scheduler->tasks;

	//check if ticks have reached the limit of uint32_t
	if (scheduler->ticks == UINT32_MAX)
		scheduler->ticks = 0;
	else
		scheduler->ticks++;

	//loop over tasks
	while (current) {
		//check if task is disabled
		if (task_get_flag(current, TASK_FLAG_DISABLED)) {
			current = current->next;
			continue;
		}

		//countdown ticks
		if (current->ticks)
			current->ticks--;

		//if wait is 0, set flag and renew the ticks
		if (current->ticks == 0) {
			task_set_flag(current, TASK_FLAG_RUN_NEXT);
			current->ticks = current->time;
		}

		current = current->next;
	}

	interrupt_clear_status();
}

//get tasks that are in the scheduler
Task *sch_tasks() {
	return scheduler->tasks;
}

void sch_init() {
	scheduler = (Scheduler *) malloc(sizeof(Scheduler));

	//set necessary values
	scheduler->ticks = 0;
	scheduler->tasks = NULL;
	scheduler->current = NULL;

	//weren't used
	scheduler->curr_priority = 0;
	scheduler->max_priority = 0;

	//add ticker as function to the interrupts
	interrupt_add_handler(sch_tick);
}
