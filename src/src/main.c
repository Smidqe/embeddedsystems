#include <stdio.h>
#include <stdlib.h>
#include "platform.h"
#include "xil_printf.h"
#include "converter/converter.h"
#include "interrupts/interrupts.h"
#include "leds/leds.h"
#include "scheduler/scheduler.h"
#include "task/task.h"
#include "state/state.h"
#include "buttons/buttons.h"
#include "uart/uart.h"
#include "pid/pid.h"
#include "semaphore/semaphore.h"
#include "pwm/pwm.h"
#include "unistd.h"

#define CHANGE_STEP 0.001
Semaphore *sm_button;
State *state;

float ref_voltage = 1; // ref voltage needed in model
char buffer[256]; // for sending to uart

void task_change_state() {
	/*Change state*/
	if (pressed(BUTTON_0) && sm_free(sm_button)) {
		sm_acquire(sm_button);
		state_next(state);

		led_disable_all();
		led_enable(3 - state_get(state));

		if (state_get(state) == STATE_CONFIGURATION)
			led_enable(LED_2 - state_pid_get(state));

		sm_release(sm_button);
	}
}

void task_change_value_sel() {
	/*Change configured parameter in configuration state*/
	if (state_get(state) != STATE_CONFIGURATION)
		return;

	if (pressed(BUTTON_1) && sm_free(sm_button)) {
		sm_acquire(sm_button);

		led_disable_all();
		led_enable(LED_3);

		// enable specific led depending on state
		state_pid_next(state);
		led_enable(LED_2 - state->pid);
		sm_release(sm_button);
	}
}

void task_change_value_conf() {
	/*Change PID-parameter values with board buttons in config state*/
	if (state_get(state) != STATE_CONFIGURATION)
		return;

	if (!(pressed(BUTTON_2) || pressed(BUTTON_3)))
		return;

	if (sm_free(sm_button) && (pressed(BUTTON_2) || pressed(BUTTON_3))) {
		sm_acquire(sm_button);

		float diff = CHANGE_STEP;

		if (pressed(BUTTON_2))
			diff *= -1;

		sprintf(buffer, "Value %d: %f -> %f \r\n", state->pid, PID_get(state->pid), PID_get(state->pid) + diff);
		uart_put_str(buffer);

		switch(state->pid) {
			case PID_KP: PID_add(PID_KP, diff); break;
			case PID_KI: PID_add(PID_KI, diff); break;
			case PID_KD: PID_add(PID_KD, diff); break;
		}

		sm_release(sm_button);
	}
}

void task_change_value_ref() {
	/*Change reference voltage with board buttons in modulating state*/
	if (state_get(state) != STATE_MODULATING)
		return;

	if (sm_free(sm_button) && (pressed(BUTTON_2) || pressed(BUTTON_3))) {
		sm_acquire(sm_button);

		float diff = CHANGE_STEP;

		if (pressed(BUTTON_2))
			diff *= -1;

		sprintf(buffer, "Reference voltage: %f -> %f \r\n", ref_voltage, ref_voltage + diff);
		ref_voltage += diff;
		uart_put_str(buffer);
		sm_release(sm_button);
	}
}

void uart_config() {
	/*Set values for PID-paramters and voltage from terminal*/
	if (!sm_free(sm_button)) return;

	uart_get_str(buffer);
	if (!*buffer) return;

	sm_acquire(sm_button);

	char *pid = strtok(buffer, " ");
	char *value = strtok(NULL, " ");

	if (!strncmp(pid, "p", 1))
		PID_set(PID_KP, atof(value));

	if (!strncmp(pid, "i", 1))
		PID_set(PID_KI, atof(value));

	if (!strncmp(pid, "d", 1))
		PID_set(PID_KD, atof(value));

	if (!strncmp(pid, "b", 1))
		PID_set(PID_BIAS, atof(value));

	if (!strncmp(pid, "r", 1))
		ref_voltage = atof(value);

	if (!strncmp(pid, "-", 1)){
		sprintf(
			buffer, "PID_KP: %7.4f\r\nPID_KI: %7.4f\r\nPID_KD: %7.4f\r\nBIAS:   %7.4f\r\nREF_V:  %7.4f\r\n",
			PID_get(PID_KP), PID_get(PID_KI), PID_get(PID_KD), PID_get(PID_BIAS), ref_voltage
		);
		uart_put_str(buffer);
		sleep(1);
	}

	sm_release(sm_button);
}


void task_model_next(){
	/*Calculate new values for model*/
	if (state_get(state) != STATE_MODULATING)
		return;

	static float out = 0;
	float u_in = 0;

	u_in = PID_calculate(ref_voltage, out);
	out = model_next(u_in);
	sprintf(buffer, "U_in: %.2f, Out: %.2f\r\n", u_in, out);
	uart_put_str(buffer);

	pwm_set(u_in);
}

void init() {
	/*Initialize uart, interrupts, leds, buttons, scheduler and create tasks*/
	uart_init();
    interrupt_init();

    sch_init();

	sm_button = sm_create();
	state = state_create();

	state_set(state, STATE_CONFIGURATION);

    sch_add_task(task_create(0, 1, 3, task_change_state));
    sch_add_task(task_create(1, 1, 2, task_change_value_conf));
    sch_add_task(task_create(2, 1, 2, task_change_value_sel));
    sch_add_task(task_create(3, 1, 2, task_change_value_ref));
    sch_add_task(task_create(4, 1, 2, task_model_next));

    led_init();
    buttons_init();

    //set first set of leds
    led_enable(LED_3);
    led_enable(LED_2);

    pwm_init();
}


void mainloop() {
	while (1) {
		Task *task = sch_next();

		if (!task)
			continue;

		/*If current state is not idle allow configuration from terminal*/
		if (state_get(state) != STATE_IDLE)
			uart_config();

		task_clear_flag(task, TASK_FLAG_RUN_NEXT);
		task_set_flag(task, TASK_FLAG_RUNNING);

		// run task
		switch (task->pid) {
			case 0: task_change_state(); break;
			case 1: task_change_value_conf(); break;
			case 2: task_change_value_sel(); break;
			case 3: task_change_value_ref(); break;
			case 4: task_model_next(); break;
		}

		task_clear_flag(task, TASK_FLAG_RUNNING);
	}
}

int main()
{

	init();
	mainloop();

    return 0;
}
