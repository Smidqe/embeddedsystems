/*
 * semaphore.c
 *
 *  Created on: Dec 7, 2018
 *      Author: j9497
 */
#include "semaphore.h"
#include "stdio.h"
#include "../uart/uart.h"
/*
	Acquire or take the semaphore,
	utilises C11 atomic operations to implement simple semaphore
	as it can test the flag and then set it atomically
*/
void sm_acquire(Semaphore *sm) {
	while (atomic_flag_test_and_set(&sm->flag) >= 1);
}

int sm_free(Semaphore *sm) {
	return sm->locked == 0;
}

/*
	Release or give the semaphore by setting the atomic flag to 0 (clearing it)
*/
void sm_release(Semaphore *sm) {
	atomic_flag_clear(&sm->flag);
}

/*
	Creates the semaphore
*/
Semaphore* sm_create() {
	Semaphore *sm = (Semaphore *) malloc(sizeof(Semaphore));

	sm->flag = (atomic_flag) ATOMIC_FLAG_INIT;
	sm->locked = 0;

	atomic_flag_clear(&sm->flag);

	return sm;
}

/*
 	Free the memory occupied by the semaphore
*/
void sm_destroy(Semaphore *sm) {
	free(sm);
}
