/*
 * semaphore.h
 *
 *  Created on: Dec 7, 2018
 *      Author: j9497
 */

#ifndef SRC_SEMAPHORE_SEMAPHORE_H_
#define SRC_SEMAPHORE_SEMAPHORE_H_

#include <stdatomic.h>
#include <xil_exception.h>
#include <stdlib.h>
#include <stdint.h>

typedef struct semaphore {
	atomic_flag flag;
	uint8_t locked;
} Semaphore;

int sm_free(Semaphore *sm);
void sm_acquire(Semaphore *sm);
void sm_release(Semaphore *sm);
Semaphore* sm_create();

#endif /* SRC_SEMAPHORE_SEMAPHORE_H_ */
