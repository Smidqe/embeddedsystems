/*
 * buttons.h
 *
 *  Created on: 7.12.2018
 *      Author: m6131
 */

#ifndef SRC_BUTTONS_BUTTONS_H_
#define SRC_BUTTONS_BUTTONS_H_

typedef enum button {
	BUTTON_0,
	BUTTON_1,
	BUTTON_2,
	BUTTON_3
} Button;

void buttons_init();
int pressed(Button b);
int pressed_all();

#endif /* SRC_BUTTONS_BUTTONS_H_ */
