/*
 * buttons.c
 *
 *  Created on: 7.12.2018
 *      Author: m6131
 */

#include "buttons.h"
#include "zynq_registers.h"

void buttons_init(){
	// Set buttons (bits 4:7) as input, leave other assignments as they are
	GPIO_DIRM_2 &= 0xFF0F;
}

int pressed(Button b){
	// Return true if the specified button is pressed
	return GPIO_DATA_2_RO & (0x10 << b);
}

int pressed_all(){
	// Return a bit field of all buttons that are pressed
	return (GPIO_DATA_2_RO & 0xF0) >> 4;
}
