/*
 * uart.c
 *
 *  Created on: 22.1.2019
 *      Author: m6131
 */

#include <stdint.h>
#include <xparameters.h>
#include <xuartps_hw.h>
#include "zynq_registers.h"

#define UART_BASE 		XPS_UART1_BASEADDR
#define UART_CTRL 		POINTER_TO_REGISTER(UART_BASE + XUARTPS_CR_OFFSET)
#define UART_MODE 		POINTER_TO_REGISTER(UART_BASE + XUARTPS_MR_OFFSET)
#define UART_BAUD_GEN 	POINTER_TO_REGISTER(UART_BASE + XUARTPS_BAUDGEN_OFFSET)
#define UART_BAUD_DIV 	POINTER_TO_REGISTER(UART_BASE + XUARTPS_BAUDDIV_OFFSET)
#define UART_FIFO 		POINTER_TO_REGISTER(UART_BASE + XUARTPS_FIFO_OFFSET)
#define UART_STATUS 	POINTER_TO_REGISTER(UART_BASE + XUARTPS_SR_OFFSET)

#include "uart.h"

void uart_init(void){
	/* Initialise uart communication */
	uint32_t r = UART_CTRL;
	r &= ~(XUARTPS_CR_TX_EN | XUARTPS_CR_RX_EN);
	r |= XUARTPS_CR_RX_DIS | XUARTPS_CR_TX_DIS;
	UART_CTRL = r;

	UART_MODE = 0;
	UART_MODE &= ~XUARTPS_MR_CLKSEL;
	UART_MODE |= XUARTPS_MR_CHARLEN_8_BIT;
	UART_MODE |= XUARTPS_MR_PARITY_NONE;
	UART_MODE |= XUARTPS_MR_STOPMODE_1_BIT;
	UART_MODE |= XUARTPS_MR_CHMODE_NORM;

	UART_BAUD_DIV = 6;
	UART_BAUD_GEN = 124;

	UART_CTRL |= (XUARTPS_CR_TXRST | XUARTPS_CR_RXRST);

	r = UART_CTRL;
	r |= XUARTPS_CR_RX_EN | XUARTPS_CR_TX_EN;
	r &= ~(XUARTPS_CR_RX_DIS | XUARTPS_CR_TX_DIS);
	UART_CTRL = r;
}

void uart_put_char(char c){
	/* Send one character */
	while (UART_STATUS & XUARTPS_SR_TNFUL);
	UART_FIFO = c;
	while (UART_STATUS & XUARTPS_SR_TACTIVE);
}

void uart_put_str(char *str){
	/* Send string until null terminator */
	while (*str) {
		uart_put_char(*str);
		str++;
	}
}

char uart_get_char(void){
	/* Get single character or null terminator */
	if (UART_STATUS & XUARTPS_SR_RXEMPTY)
		return 0;

	return UART_FIFO;
}

void uart_get_str(char *buffer) {
	/* Read string until null terminator */
	while((*(buffer++) = uart_get_char()));
}
