/*
 * uart.h
 *
 *  Created on: 22.1.2019
 *      Author: m6131
 */

#ifndef SRC_UART_UART_H_
#define SRC_UART_UART_H_

void uart_put_char(char);
void uart_put_str(char*);
char uart_get_char(void);
void uart_get_str(char*);
void uart_init();

#endif /* SRC_UART_UART_H_ */
