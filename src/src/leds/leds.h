/*
 * leds.h
 *
 *  Created on: Nov 27, 2018
 *      Author: j9497
 */

#ifndef SRC_LEDS_H_
#define SRC_LEDS_H_

#define LED_BANK_SHIFT 8
#define LED_BIT_ALL_MASK 0b1111
#define LED_COUNT 4

typedef enum led {
	LED_0,
	LED_1,
	LED_2,
	LED_3
} Led;

void led_disable(Led led);
void led_enable(Led led);
int led_mask(Led led);
void led_toggle(Led led);
void led_init();
void led_disable_all();
void led_enable_all();

#endif /* SRC_LEDS_H_ */
