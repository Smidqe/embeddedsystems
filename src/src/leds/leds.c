/*
	Handles the controlling the leds
	Author: j9497
*/
#include "zynq_registers.h"
#include "leds.h"

#define LED_BANK_SHIFT 8
#define LED_BIT_ALL_MASK 0b1111
#define LED_COUNT 4

//get the led mask
int led_mask(Led led) {
	return (1 << led) << LED_BANK_SHIFT;
}

//mask all leds
int led_mask_all() {
	return LED_BIT_ALL_MASK << 8;
}

//set the led on or off depending on the value
void led_data(Led led, int value) {
	int mask = led_mask(led);

	if (value)
		GPIO_DATA_2 |= mask;
	else
		GPIO_DATA_2 &= ~mask;
}

//enable (switch on) a led
void led_enable(Led led) {
	led_data(led, 1);
}

//disable (switch off) a led
void led_disable(Led led) {
	led_data(led, 0);
}

//toggle the led
void led_toggle(Led led) {
	GPIO_DATA_2 ^= led_mask(led);
}

//disable (switch off) all leds
void led_disable_all() {
	GPIO_DATA_2 &= ~led_mask_all();
}

//enable (switch on) all leds
void led_enable_all() {
	GPIO_DATA_2 |= led_mask_all();
}

//initialise necessary registers
void led_init() {
	GPIO_OEN_2 |= LED_BIT_ALL_MASK << 8;
	GPIO_DIRM_2 |= LED_BIT_ALL_MASK << 8;
}
