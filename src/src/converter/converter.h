/*
 * converter.h
 *
 *  Created on: 30.11.2018
 *      Author: m6131
 */

#ifndef SRC_CONVERTER_H_
#define SRC_CONVERTER_H_

void model_step(float * ans, float * state, float u_in);
float model_next(float u_in);

#endif /* SRC_CONVERTER_H_ */
