/*
 * converter.c
 *
 *  Created on: 30.11.2018
 *      Author: m6131
 */

#define model_N 6

/* --- MODEL VALUES --- */
/* Main model matrix */
float M[model_N][model_N] = {
	{0.9878, -0.0169,  0, 	    0,       0,       0},
	{0.7626,  0.4102, -0.3251,  0.2023, -0.2118,  0.3782},
	{0.5998,  1.0565, -0.4043, -0.3708,  0.3881, -0.6930},
	{0.4355,  0.7672,  0.4326, -0.5470, -0.4337,  0.7745},
	{0.3907,  0.6883,  0.3881,  0.3718, -0.4037, -1.0648},
	{0.2101,  0.3700,  0.2087,  0.1999,  0.3206,  0.4275}
};

/* Multipliers to input voltage */
float c[model_N] = {0.02424, 0.0093, 0.007315, 0.005312, 0.004765, 0.002562};

/* Two-part model_state to save the last available value while calculating the next */
float model_state[2][model_N] = {
	{0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0}
};
/* --- END MODEL --- */

void model_step(float * ans, float * model_state, float u_in){
	/* Calculate the matrix multiplication and summation in the converter model */
	for(int i = 0; i < model_N; i++){
		float row = 0;
		for(int j = 0; j < model_N; j++){
			row += M[i][j] * (*(model_state + j));
		}
		*(ans + i) = row + u_in * c[i];
	}
}

float model_next(float u_in){
	/* Advance the converter with one step based on input voltage */
	static unsigned int flip = 1;
	flip ^= 1;

	model_step(&model_state[flip ^ 1][0], &model_state[flip][0], u_in);

	return model_state[flip ^ 1][model_N-1];
}
