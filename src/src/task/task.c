/*
 * task.c
 *
 *  Created on: Dec 7, 2018
 *      Author: smidqe
 */

//What to include here???
#include <stdint.h>
#include <stdlib.h>
#include "task.h"

int task_get_flag(Task *task, Taskflag flag) {
	return task->flags & (1 << flag);
}

void task_set_flag(Task *task, Taskflag flag) {
	task->flags |= (1 << flag);
}

void task_clear_flag(Task *task, Taskflag flag) {
	task->flags &= ~(1 << flag);
}

Task* task_create(uint32_t pid, uint8_t priority, uint32_t ticks, void *func) {
	Task *task = (Task *) malloc(sizeof(Task));

	task->pid = pid;
	task->priority = priority;
	task->ticks = ticks;
	task->time = ticks;
	task->func = func;
	task->next = NULL;
	task->flags = 0;

	return task;
}

void task_remove(Task *task) {
	free(task);
}

int task_is_runnable(Task *task) {
	return task->flags & TASK_MASK_RUNNABLE;
}

int task_running(Task *task) {
	return task->flags & TASK_FLAG_RUNNING;
}

void task_free(Task *task) {
	free(task);
}
