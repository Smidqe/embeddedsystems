/*
 * task.h
 *
 *  Created on: Dec 7, 2018
 *      Author: smidqe
 */

#ifndef SRC_TASK_TASK_H_
#define SRC_TASK_TASK_H_

typedef enum taskflag {
	TASK_FLAG_RUNNING,
	TASK_FLAG_DISABLED,
	TASK_FLAG_EXECUTED,
	TASK_FLAG_TIMEOUT,
	TASK_FLAG_RUN_NEXT
} Taskflag;

#define TASK_MASK_RUNNABLE 0b11
/*
	For the Task struct and it's flags variable

	0: running/not running
	1: disabled
	2: executed

*/

typedef struct task {
	uint32_t pid;

	//amount of ticks it should last
	uint32_t ticks;

	//how long it should last?
	uint32_t time;
	//flags for running/waiting etc
	uint8_t flags;

	//priority from 0..255
	uint8_t priority;

	void *func; //???
	void *data; //???

	//next one
	struct task *next;
} Task;

Task* task_create(uint32_t pid, uint8_t priority, uint32_t ticks, void *func);
void task_allocate();
void task_free();

void task_set_flag(Task *task, Taskflag flag);
int task_get_flag(Task *task, Taskflag flag);
void task_clear_flag(Task *task, Taskflag flag);

uint8_t task_get_priority(Task *task);
uint8_t task_set_priority(Task *task, uint8_t priority);

void *task_get_function(Task *task);
void *task_set_function(Task *task, void *func);

uint32_t task_get_ticks(Task *task);
void task_set_ticks(Task *task, uint32_t ticks);

uint32_t task_inc_tick(Task *task);
int task_is_runnable(Task *task);
int task_running(Task *task);

#endif /* SRC_TASK_TASK_H_ */
