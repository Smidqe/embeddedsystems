/*
 * pid.h
 *
 *  Created on: 14.12.2018
 *      Author: m6131
 */

#ifndef SRC_PID_PID_H_
#define SRC_PID_PID_H_

#include "stdlib.h"

/* Limits for PID controller output */
#define PID_MAX 10
#define PID_MIN -10

typedef enum {
	PID_KP,
	PID_KI,
	PID_KD,
	PID_BIAS
} PID_param;

float PID_calculate(float reference, float actual);
float PID_get(PID_param param);
void PID_set(PID_param param, float value);
void PID_add(PID_param param, float value);

#endif /* SRC_PID_PID_H_ */
