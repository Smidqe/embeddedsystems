/*
 * pid.c
 *
 *  Created on: 14.12.2018
 *      Author: m6131
 */

#include <math.h>
#include "pid.h"

/* Controller constants, ITER_TIME based on 50 kHz sampling time */
#define EPSILON 1E-4
#define ITER_TIME 0.00002
#define INT_LIMIT 100

float PID_p = 1;
float PID_i = 0.002;
float PID_d = -0.5;
float PID_bias = 0.65;

float clip(float value, float lower, float upper){
	/* Restrict values to range lower..upper */
	if (value < lower)
		return lower;

	if (upper < value)
		return upper;

	return value;
}

float PID_calculate(float reference, float in){
	/* Calculate PID controller output voltage and manage state */
	static float error_old = 0;
	static float integral = 0;

	float error = reference - in;
	float derivative = (error - error_old);

	if(fabs(error) > EPSILON)
		integral += error;

    /* Anti windup */
    integral = clip(integral, -INT_LIMIT, INT_LIMIT);

	float out = PID_p * error + PID_i * integral - PID_d * derivative + PID_bias;

	/* Saturation */
	out = clip(out, PID_MIN, PID_MAX);

	error_old = error;
	return out;
}

void PID_set(PID_param param, float value){
	switch(param){
		case PID_KP: PID_p = value; break;
		case PID_KI: PID_i = value; break;
		case PID_KD: PID_d = value; break;
		case PID_BIAS: PID_bias = value;
	}
}

float PID_get(PID_param param){
	switch(param){
		case PID_KP: return PID_p;
		case PID_KI: return PID_i;
		case PID_KD: return PID_d;
		case PID_BIAS: return PID_bias;
	}
	return 0;
}

void PID_add(PID_param param, float value){
	switch(param){
		case PID_KP: PID_p += value; break;
		case PID_KI: PID_i += value; break;
		case PID_KD: PID_d += value; break;
		case PID_BIAS: PID_bias += value;
	}
}
