#include "xttcps.h"
#include "zynq_registers.h"
#include "sleep.h"
#include "stdint.h"
#include "../pid/pid.h"
#include "pwm.h"

void pwm_init(){
	// Initialise triple time counters for pwm leds
	TTC0_CLK_CNTRL2 = (1 << XTTCPS_CLK_CNTRL_PS_VAL_SHIFT) | XTTCPS_CLK_CNTRL_PS_EN_MASK; // Set identical to TTC0_CLK_CNTRL
	TTC0_CLK_CNTRL3 = TTC0_CLK_CNTRL2;

	TTC0_CNT_CNTRL2  = XTTCPS_CNT_CNTRL_RST_MASK | XTTCPS_CNT_CNTRL_DIS_MASK | XTTCPS_CNT_CNTRL_MATCH_MASK | XTTCPS_CNT_CNTRL_POL_WAVE_MASK;
	TTC0_CNT_CNTRL3 = TTC0_CNT_CNTRL2;

	TTC0_CNT_CNTRL2 &= ~XTTCPS_CNT_CNTRL_DIS_MASK;
	TTC0_CNT_CNTRL3 &= ~XTTCPS_CNT_CNTRL_DIS_MASK;
}

void pwm_set(float pid){
	if (pid >= 0){
		// Blue led
		TTC0_MATCH_1_COUNTER_2 = 0;
		TTC0_MATCH_1_COUNTER_3 = (int) (pid*(65535 / PID_MAX));
	} else {
		// Green led
		TTC0_MATCH_1_COUNTER_3 = 0;
		TTC0_MATCH_1_COUNTER_2 = (int) (pid*(65535 / PID_MIN));
	}
}
