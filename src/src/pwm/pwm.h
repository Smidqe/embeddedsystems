/*
 * pwm.h
 *
 *  Created on: Jan 28, 2019
 *      Author: smidqe
 */

#ifndef SRC_PWM_PWM_H_
#define SRC_PWM_PWM_H_

void pwm_init();
void pwm_set(float pid);

#endif /* SRC_PWM_PWM_H_ */
