/*
 * state.c
 *
 *  Created on: Jan 11, 2019
 *      Author: j9497
 */

#include "state.h"

//set new state
void state_set(State *state, States new) {
	if (state->state == new)
		return;

	state->state = new;
}

//get current state
States state_get(State *state) {
	return state->state;
}

//set next state, changes by +1 and wraps around
void state_next(State *state) {
	int id = state->state;

	//check if we have reached the highest state
	if (id + 1 > STATE_MODULATING)
		id = 0;
	else
		id++;

	state_set(state, id);
}

//create the state
State *state_create() {
	State *state = malloc(sizeof(State));

	//set necessary values
	state->state = STATE_CONFIGURATION;
	state->pid = PID_KP;

	state->prevVal = 0;
	state->pidVal = 0;

	return state;
}

//free the memory of the state
void state_free(State *state) {
	free(state);
}

//set pid state
void state_pid_set(State *state, PID_param newstate) {
	state->pid = newstate;
}

//get pid state
PID_param state_pid_get(State *state) {
	return state->pid;
}

//set next pid state, wraps
void state_pid_next(State *state) {
	if (state->pid == PID_KD)
		state->pid = PID_KP;
	else
		state->pid++;
}
