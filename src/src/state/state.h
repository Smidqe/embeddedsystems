/*
 * state.h
 *
 *  Created on: Jan 11, 2019
 *      Author: j9497
 */

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include "../pid/pid.h"

#ifndef SRC_STATE_STATE_H_
#define SRC_STATE_STATE_H_

typedef enum states {
	STATE_CONFIGURATION,
	STATE_IDLE,
	STATE_MODULATING
} States;

typedef struct state {
	uint8_t state;
	uint8_t pid;

	float prevVal;
	float pidVal;
} State;

void state_set(State *state, States new);
States state_get(State *state);
void state_next(State *state);


State *state_create();
void state_free(State *state);

void state_pid_next(State *state);
PID_param state_pid_get(State *state);
void state_pid_set(State *state, PID_param newstate);

#endif /* SRC_STATE_STATE_H_ */
