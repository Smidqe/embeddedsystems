/*
 * interrupts.h
 *
 *  Created on: Dec 14, 2018
 *      Author: j9497
 */

#ifndef SRC_INTERRUPTS_INTERRUPTS_H_
#define SRC_INTERRUPTS_INTERRUPTS_H_

#include <xparameters.h>

void interrupt_add_handler();
void interrupt_init();
void interrupt_clear_status();

#endif /* SRC_INTERRUPTS_INTERRUPTS_H_ */
