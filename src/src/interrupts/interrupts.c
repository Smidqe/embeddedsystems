/*
 * interrupts.c
 *
 *  Created on: Dec 13, 2018
 *      Author: j9497
 */
#include <zynq_registers.h>
#include <xscugic.h>
#include <xuartps_hw.h>
#include <xttcps.h>

#define TTC_TICK_DEVICE_ID 	XPAR_XTTCPS_0_DEVICE_ID
#define TTC_TICK_INTR_ID  	XPAR_XTTCPS_0_INTR
#define INTC_DEVICE_ID      XPAR_SCUGIC_SINGLE_DEVICE_ID

XScuGic InterruptController;

void interrupt_set_timer() {
    TTC0_CNT_CNTRL |= XTTCPS_CNT_CNTRL_DIS_MASK; // Disable counter
    // Reset the count control register to it's default value.
    TTC0_CNT_CNTRL = XTTCPS_CNT_CNTRL_RESET_VALUE;
    // Reset the rest of the registers to the default values.
    TTC0_CLK_CNTRL = 0;
    TTC0_INTERVAL_VAL = 0;
    TTC0_MATCH_1 = 0;
    TTC0_MATCH_2_COUNTER_2 = 0;
    TTC0_MATCH_3_COUNTER_2 = 0;
    TTC0_IER = 0;
    TTC0_ISR = XTTCPS_IXR_ALL_MASK;

    // Reset the counter value
    TTC0_CNT_CNTRL |= XTTCPS_CNT_CNTRL_RST_MASK;

    // Set the options
    TTC0_CNT_CNTRL = XTTCPS_CNT_CNTRL_DIS_MASK | XTTCPS_CNT_CNTRL_INT_MASK;

    // Set the interval and prescale. Base clock is 111MHz
    // Prescale value (N): if prescale is enabled, the  count rate is divided by 2^(N+1)
    // 1 / (111MHz) * 9000 * 2^(11+1) = 0.3321... [seconds]
    TTC0_INTERVAL_VAL = 2000;
    TTC0_CLK_CNTRL &= ~(XTTCPS_CLK_CNTRL_PS_VAL_MASK | XTTCPS_CLK_CNTRL_PS_EN_MASK);
    TTC0_CLK_CNTRL |= (11 << XTTCPS_CLK_CNTRL_PS_VAL_SHIFT) | XTTCPS_CLK_CNTRL_PS_EN_MASK;
}

void interrupt_setup(XScuGic *IntcInstancePtr) {
    //XScuGic_Config structure contains configuration information for the device
    XScuGic_Config IntcConfig_instance; // Initialize structure of type XScuGic_Config - EDIT 2018.11.23: This instance initialization was missing
    XScuGic_Config * IntcConfig = &IntcConfig_instance; // These could be also declared global

    // Initialize the interrupt controller driver (XScuGic_Config structure)
    IntcConfig->DeviceId = XPAR_PS7_SCUGIC_0_DEVICE_ID;
    IntcConfig->CpuBaseAddress = XPAR_PS7_SCUGIC_0_BASEADDR;
    IntcConfig->DistBaseAddress = XPAR_PS7_SCUGIC_0_DIST_BASEADDR;
    IntcConfig->HandlerTable[INTC_DEVICE_ID].CallBackRef = NULL;
    IntcConfig->HandlerTable[INTC_DEVICE_ID].Handler = NULL;

    /**
    * CfgInitialize a specific interrupt controller instance/driver. The
    * initialization entails:
    *
    * - Initialize fields of the XScuGic structure
    * - Initial vector table with stub function calls
    * - All interrupt sources are disabled
    */
    XScuGic_CfgInitialize(IntcInstancePtr, IntcConfig, IntcConfig->CpuBaseAddress);
    // Connect the interrupt controller interrupt handler to the hardware interrupt handling logic in the ARM processor.
    Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_IRQ_INT, (Xil_ExceptionHandler) XScuGic_InterruptHandler, IntcInstancePtr);
    // Enable interrupts in the ARM
    Xil_ExceptionEnable();
}

void interrupt_setup_ticker() {
	// Connect to the interrupt controller
	//InterruptController.Config->HandlerTable[TTC_TICK_INTR_ID].Handler = NULL;
	// Enable the interrupt for the Timer counter
	ICDISER1 = 1<<(TTC_TICK_INTR_ID % 32);			// XScuGic_Enable(&InterruptController, TTC_TICK_INTR_ID);

	// Enable the interrupts for the tick timer/counter. We only care about the interval timeout.
	TTC0_IER |= XTTCPS_IXR_INTERVAL_MASK;			// XTtcPs_EnableInterrupts(TtcPsTick, XTTCPS_IXR_INTERVAL_MASK);
	// Start the tick timer/counter
	TTC0_CNT_CNTRL &= ~XTTCPS_CNT_CNTRL_DIS_MASK;	// XTtcPs_Start(TtcPsTick);
}

void interrupt_add_handler(void *func) {
	//insert the function ast the handler for interrupts
	InterruptController.Config->HandlerTable[TTC_TICK_INTR_ID].Handler = (Xil_InterruptHandler) func;
}

void interrupt_clear_status() {
	TTC0_ISR; //read the isr register
}

void interrupt_init() {
	//setup the interrupt with the controller
	interrupt_setup(&InterruptController);

	//set the timer
	interrupt_set_timer();

	//set the ticker
	interrupt_setup_ticker();
}
