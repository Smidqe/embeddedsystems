# Authors:  
- Felix Hild�n,   
- Miikka Lahtinen,  
- Markus Lind�n,  
- Camilla Piskonen  
  
# Basics:
- Starting state: Configuration
- Other states are same as instructed
- Buttons work as instructed
- Leds indicate current state as instructed

In configuration mode LED_3 is on and other leds indicate selected parameter:
LED_0 is PID_P, LED_1 is PID_I, LED_2 is PID_D
BTN_3 and BTN_2 are used to change values for parameter and BTN_1 toggles between parameteres
When parameter value is changed a message to Xilinx console is sent

In idling mode LED_2 is on

In modulating mode LED_1 is on and RGB_LED visualizes the controller output, green is negative and blue positive
BTN_3 and BTN_2 are used to change value for reference voltage
When reference voltage is changed a message to Xilinx console is sent

User can also change values from command line when not in idle:

# Serial commands
- "p <float>" – sets value for P
- "i <float>" – sets value for I
- "d <float>" – sets value for D
- "b <float>" – sets value for bias
- "r <float>" – sets value for reference voltage
- "-"	    – prints all parameters




